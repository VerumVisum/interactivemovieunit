﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

[CustomEditor(typeof(ButtonVisualChanger), true)]
//[CanEditMultipleObjects]
public class VisualChangerCustomEditor : Editor {

	public override void OnInspectorGUI () {
		base.OnInspectorGUI ();

		ButtonVisualChanger bvc = (ButtonVisualChanger)target;
		//EditorGUILayout.ObjectField("Press animation", bvc.PressAnimationName, typeof(string), true);
		if(bvc.AnimatedButton) {
			bvc.PressAnimationName = EditorGUILayout.TextField("Press animation", bvc.PressAnimationName);
			bvc.PressedAnimationName = EditorGUILayout.TextField("Pressed animation", bvc.PressedAnimationName);
			bvc.UnpressedAnimationName = EditorGUILayout.TextField("Unpressed animation", bvc.UnpressedAnimationName);
		}
		if (bvc.gameObject.GetComponent<MeshRenderer> () != null ||
		   bvc.gameObject.GetComponent<SpriteRenderer> () != null ||
		   bvc.gameObject.GetComponent<Image> () != null) {
			bvc.PressedColor = EditorGUILayout.ColorField ("Pressed color", bvc.PressedColor);
			bvc.PressColor = EditorGUILayout.ColorField ("Press color", bvc.PressColor);
			bvc.UnpressedColor = EditorGUILayout.ColorField ("UnPressed color", bvc.UnpressedColor);
		}
	}
}
